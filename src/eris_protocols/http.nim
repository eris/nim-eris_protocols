# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Isc

# TODO: add a logging callback

import std/[asyncdispatch, asyncnet, parseutils, net, strutils, uri]
import ./private/asynchttpserver
import eris

const n2rPath = "/uri-res/N2R"

type StoreServer* = ref object
  store: ErisStore
  http: AsyncHttpServer

using server: StoreServer

proc userAgent*(req: Request): string =
  $req.headers.getOrDefault("user-agent")

proc newServer*(store: ErisStore): StoreServer =
  StoreServer(store: store, http: newAsyncHttpServer())

proc erisCap(req: Request): ErisCap =
  let elems = req.url.path.split '/'
  if elems.len != 2:
    raise newException(ValueError, "bad path " & req.url.path)
  parseErisUrn elems[1]

proc parseRange(range: string): tuple[a: BiggestInt, b: BiggestInt] =
  ## Parse an HTTP byte range string.
  if range != "":
    var start = skip(range, "bytes=")
    if start > 0:
      start.inc parseBiggestInt(range, result.a, start)
      if skipWhile(range, {'-'}, start) == 1:
        discard parseBiggestInt(range, result.b, start+1)

proc getBlock(server; req: Request; `ref`: Reference): Future[void] {.async.} =
  var
    blk = await server.store.get(`ref`)
    headers = newHttpHeaders({
      "content-type": "application/octet-stream"})
  await req.respond(Http200, cast[string](blk), headers)

proc getContent(server; req: Request; cap: ErisCap): Future[void] {.async.} =
  var
    stream = newErisStream(server.store, cap)
    totalLength = int(await stream.length)
    (startPos, endPos) = req.headers.getOrDefault("range").parseRange
  if endPos == 0 or endPos > startPos:
      endPos = pred totalLength
  var
    remain = succ(endPos - startPos)
    buf = newSeq[byte](min(remain, cap.blockSize.int))
    headers = newHttpHeaders({
      "connection": "close",
      "content-length": $remain,
      "content-range": "bytes $1-$2/$3" % [ $startPos, $endPos, $totalLength ],
      "content-type": "application/octet-stream"
    })
  await req.respond(Http206, "", headers)
  stream.setPosition(BiggestUInt startPos)
  var n = int min(buf.len, remain)
  if (remain > cap.blockSize.int) and ((startPos and cap.blockSize.int.pred) != 0):
    # shorten the first read to align the stream
    n.dec(startPos.int and cap.blockSize.int.pred)
  try:
    while remain > 0 and not req.client.isClosed:
      n = await stream.readBuffer(addr buf[0], n)
      if n > 0:
        await req.client.send(addr buf[0], n, {})
        remain.dec(n)
        n = int min(buf.len, remain)
      else:
        break
  except: discard
  close(req.client)
  close(stream)

proc get(server; req: Request): Future[void] =
  const
    blockPrefix = "urn:blake2b:"
    contentPrefix = "urn:eris"
  if req.url.path == n2rPath:
    if req.url.query.startsWith(blockPrefix):
      var r: Reference
      if r.fromBase32(req.url.query[blockPrefix.len..req.url.query.high]):
        result = getBlock(server, req, r)
      else:
        result = req.respond(Http400, "invalid block reference")
    elif req.url.query.startsWith contentPrefix:
      var cap = parseErisUrn req.url.query
      result = getContent(server, req, cap)
  if result.isNil:
    result = req.respond(Http403, "invalid path or query for this server")

proc head(server; req: Request): Future[void] {.async.} =
  ## Check that ERIS data is available.
  var
    cap = req.erisCap
    stream = newErisStream(server.store, cap)
    len = await stream.length()
    headers = newHttpHeaders({
      "Accept-Ranges": "bytes",
      "Content-Length": $len})
  await req.respond(Http200, "", headers)

proc put(server; req: Request): Future[void] {.async.} =
  let blockSize =
    if req.body.len < (1024 shl 16): bs1k
    else: bs32k
  var cap = await server.store.encode(blockSize, req.body)
  await req.respond(Http200, "", newHttpHeaders({
    "content-location": n2rPath & "?" & $cap}))

proc serve*(server: StoreServer; ops = {Get, Put};
    ipAddr = parseIpAddress("::"); port = Port(80)): Future[void] =
  proc handleRequest(req: Request) {.async.} =
    try:
      case req.reqMethod
      of HttpGET:
        if Get in ops:
          await server.get(req)
          return
      of HttpHEAD:
        if Get in ops:
          await server.head(req)
          return
      of HttpPUT:
        if Put in ops:
          await server.put(req)
          return
      else: discard
      await req.respond(Http403, "method not allowed")
    except KeyError, IOError:
      await req.respond(Http404, getCurrentExceptionMsg())
    except ValueError:
      await req.respond(Http400, getCurrentExceptionMsg())
    except:
      if not req.client.isClosed:
        await req.respond(Http500, getCurrentExceptionMsg())
  let domain =
    case ipAddr.family
    of IpAddressFamily.IPv6: AF_INET6
    of IpAddressFamily.IPv4: AF_INET
  server.http.serve(port, handleRequest, $ipAddr, domain = domain)

proc close*(server: StoreServer) =
  close(server.http)
