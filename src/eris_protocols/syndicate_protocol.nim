# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

include ./private/syndicate_protocol
  # generated schema

import std/asyncfutures
from std/sequtils import toSeq
import syndicate
from syndicate/actors import preventInertCheck
from syndicate/protocols/dataspace import Observe
from syndicate/patterns import DLit
import eris

proc fromPreserveHook*[E](v: var Reference; pr: Preserve[E]): bool =
  if pr.kind == pkByteString and pr.bytes.len == v.bytes.len:
    copyMem(addr v.bytes[0], unsafeAddr pr.bytes[0], v.bytes.len)
    result = true

proc fromPreserveHook*[E](v: var Operations; pr: Preserve[E]): bool =
  if pr.isSet:
    result = true
    for pe in pr.set:
      if pe.isSymbol "Get":
        v.incl Get
      elif pe.isSymbol "Put":
        v.incl Put
      else:
        result = false

proc toPreserveHook*(r: Reference; E: typedesc): Preserve[E] =
  ## Hook for preserving `Reference`.
  Preserve[E](kind: pkByteString, bytes: r.bytes.toSeq)

type SyndicateStore* {.final.} = ref object of ErisStoreObj
  facet: Facet
  ds: Ref
  disarm: proc() {.gcsafe.}

proc run(store: SyndicateStore, action: TurnAction) =
  ## Run an action in a new facet.
  store.facet.run do (turn: var Turn):
    discard facet(turn, action)

method get(store: SyndicateStore; blkRef: Reference): Future[seq[byte]] =
  let fut = newFuture[seq[byte]]("SyndicateStore.get")
  store.run do (turn: var Turn):
    let pat = ErisBlock ? { 0: ?blkRef, 1: grab() }
    onPublish(turn, store.ds, pat) do (blk: seq[byte]):
      verifyBlock(blkRef, blk)
      fut.complete(blk)
      stop(turn)
  fut

method hasBlock(store: SyndicateStore; blkRef: Reference): Future[bool] =
  let fut = newFuture[bool]("SyndicateStore.hasBlock")
  store.run do (turn: var Turn):
    # TODO: return false on timeout
    onPublish(turn, store.ds, ErisCache ? {0: ?blkRef}) do:
      fut.complete(true)
      stop(turn)
  fut

method put(store: SyndicateStore; blkRef: Reference; f: PutFuture) =
  store.run do (turn: var Turn):
    # assert the block until a cache assertion is present
    let pat = ErisCache ? {0: ?blkRef}
    onPublish(turn, store.ds, pat) do:
      complete(f)
      stop(turn)
    discard publish(turn, store.ds,
      ErisBlock(reference: blkRef.bytes.toSeq, content: f.mget))

method close(store: SyndicateStore) =
  store.disarm()

proc newSyndicateStore*(turn: var Turn; ds: Ref; ops: Operations): SyndicateStore =
  var store = SyndicateStore(ds: ds)
  store.facet = turn.facet do (turn: var Turn):
    store.disarm = turn.facet.preventInertCheck()
  store

proc newStoreFacet*(turn: var Turn; store: ErisStore; ds: Ref): Facet =
  facet(turn) do (turn: var Turn):
    let
      blockRequest = Observe[Ref] ? { 0: ??(ErisBlock ? { 0: ?DLit }) }
      cacheRequest = Observe[Ref] ? { 0: ??(ErisCache ? { 0: ?DLit }) }

    during(turn, ds, blockRequest) do (blkRef: Reference):
      let facet = turn.facet
      store.get(blkRef).addCallback do (blkFut: Future[seq[byte]]):
        run(facet) do (turn: var Turn):
          if not blkFut.failed:
            var blk = blkFut.read
            discard publish(turn, ds,
              ErisBlock(reference: blkRef.bytes.toSeq, content: blk))

    during(turn, ds, cacheRequest) do (blkRef: Reference):
      let facet = turn.facet
      store.hasBlock(blkRef).addCallback do (fut: Future[bool]):
        run(facet) do (turn: var Turn):
          if fut.read:
            discard publish(turn, ds, ErisCache(reference: blkRef.bytes.toSeq))
          else:
            var pat = ErisBlock ? { 0: ?blkRef.bytes.toSeq, 1: grab() }
            onPublish(turn, ds, pat) do (blk: seq[byte]):
              var putFut = newFutureVar[seq[byte]]("during(ErisCache)")
              putFut.complete(blk)
              clean(putFut)
              store.put(blkRef, putFut)
              cast[Future[void]](putFut).addCallback do (fut: Future[void]):
                run(facet) do (turn: var Turn):
                  discard publish(turn, ds, ErisCache(reference: blkRef.bytes.toSeq))
