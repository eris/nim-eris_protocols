
import
  std/typetraits, preserves

type
  ErisBlock* {.preservesRecord: "erisx3".} = object
    `reference`*: seq[byte]
    `content`*: seq[byte]

  ErisCache* {.preservesRecord: "erisx3-cache".} = object
    `reference`*: seq[byte]
    `persist`*: int

proc `$`*(x: ErisBlock | ErisCache): string =
  `$`(toPreserve(x))

proc encode*(x: ErisBlock | ErisCache): seq[byte] =
  encode(toPreserve(x))
