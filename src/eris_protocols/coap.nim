# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, random, uri]
import coap/[common, tcp]
import eris

export fromString
export serve

const pathPrefix = "erisx3"

type
  Url = common.Uri
  Uri = uri.Uri

proc fromOption(blkRef: var Reference; opt: Option): bool =
  ## Take a `Reference` value from an `Option`.
  ## Option data can be raw or in base32 form.
  case opt.data.len
  of 32: blkRef.bytes.fromOption opt
  of 52: blkRef.fromBase32 cast[string](opt.data)
  else: false

type
  StoreSession {.final.} = ref object of Session
    store*: ErisStore
    ops: Operations
  StoreServer* {.final.} = ref object of Server
    store: ErisStore
    server: Server
    ops: Operations

method createSession(server: StoreServer): Session =
  StoreSession(store: server.store, ops: server.ops)

method onMessage(session: StoreSession; req: Message) =
  var
    resp = Message(token: req.token, code: codeSuccessContent)
    prefix: string
    blkRef: Reference
    pathCount: int
  for opt in req.options:
    if opt.num == optUriPath:
      case pathCount
      of 0:
        if not prefix.fromOption opt:
          resp.code = codeBadCsmOption
      of 1:
        if not blkRef.fromOption opt:
          resp.code = codeBadCsmOption
      else: discard
      inc pathCount
  if pathCount != 2 or prefix != pathPrefix:
    resp.code = codeNotFound
  if resp.code != codeSuccessContent:
    send(session, resp)
  else:
    case req.code
    of codeGET:
      assert(eris.Operation.Get in session.ops)
      if eris.Operation.Get in session.ops:
        session.store.get(blkRef).addCallback do (blkFut: Future[seq[byte]]):
          if blkFut.failed: resp.code = codeNotFound
          else:
            var blk = read blkFut
            assert(blk.len > 0)
            resp.code = codesuccessContent
            resp.payload = blk
            assert(resp.payload.len > 0)
          send(session, resp)
        return
    of codePUT:
      assert(eris.Operation.Put in session.ops)
      if eris.Operation.Put in session.ops:
        if req.payload.len notin {bs1k.int, bs32k.int}:
          var resp = Message(code: code(4, 6), token: req.token)
          resp.payload = "PUT payload was not of a valid block size"
          send(session, resp)
        else:
          var putFut = newFutureVar[seq[byte]]"onMessage"
          putFut.complete req.payload
          clean putFut
          cast[Future[seq[byte]]](putFut).addCallback do (putFut: Future[seq[byte]]):
            var resp = Message(token: req.token)
            if putFut.failed: resp.code = code(5, 0)
            else: resp.code = codeSuccessCreated
            send(session, resp)
          session.store.put(blkRef, cast[PutFuture](putFut))
        return
    else: discard
    resp.code = codeNotMethodNotAllowed
    send(session, resp)

proc newServer*(store: ErisStore; ops = {eris.Get,eris.Put}): StoreServer =
  ## Create new `StoreServer`. The `ops` argument determines
  ## if clients can GET and PUT to the server.
  StoreServer(store: store, ops: ops)

proc close*(server: StoreServer) = stop(server)
  ## Close and stop a `StoreServer`.

type
  StoreClient* = ref StoreClientObj
  StoreClientObj = object of ErisStoreObj
    client: Client
    rng: Rand

method put(s: StoreClient; r: Reference; pFut: PutFuture) =
  var options =
    when defined(release): @[pathPrefix.toOption(optUriPath), toOption(r.bytes, optUriPath)]
    else: @[pathPrefix.toOption(optUriPath), toOption($r, optUriPath)]
  var msg = Message(
    code: codePUT,
    token: Token s.rng.rand(0xffffff),
    options: options)
  msg.payload = pFut.mget
  request(s.client, msg).addCallback do (mFut: Future[Message]):
    try:
      var resp = read mFut
      doAssert resp.token == msg.token
      doAssert resp.code == codeSuccessCreated, $resp.code
      complete pFut
    except CatchableError as e:
      fail(cast[Future[void]](pFut), e)

method get(s: StoreClient; r: Reference): Future[seq[byte]] {.async.} =
  var msg = Message(
    code: codeGet,
    token: Token s.rng.rand(0xffffff),
    options: @[pathPrefix.toOption(optUriPath), r.bytes.toOption(optUriPath)])
  var resp = await request(s.client, msg)
  doAssert resp.token == msg.token
  if resp.code != codeSuccessContent:
    raise newException(IOError, "server returned " & $resp.code)
  assert resp.payload.len in {bs1k.int, bs32k.int}, $resp.payload.len
  return resp.payload

method close(client: StoreClient) = close(client.client)

proc newStoreClient*(uri: Uri): Future[StoreClient] {.async.} =
  var url: Url
  if not url.fromUri(uri):
    raise newException(ValueError, "invalid CoAP URI")
  let client = await connect(url)
  return StoreClient(client: client, rng: initRand())
