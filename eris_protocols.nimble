# Package

version       = "0.4.2"
author        = "Endo Renberg"
description   = "ERIS protocols library"
license       = "Unlicense"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.2", "eris >= 0.4.0", "coap >= 0.0.1", "syndicate"
